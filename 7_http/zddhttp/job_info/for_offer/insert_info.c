#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <mysql/mysql.h>
#include "./base_CGI.h"
#include "./decode.h"


#define N 5
int separate_info(char buf[],char *user_name,char *company_name,char *job,char *date,char *status)
{
  char * p = buf;
  p += strlen("user_name=");
  int i = 0;
  while(*p != '&' && *p != '\0')
  {
    user_name[i++] = *p;
    p++;
  }
  user_name[i++] = '\0';
  p += strlen("company_name=");
  p++;
  i = 0;
  while(*p != '&' && *p != '\0')
  {
    company_name[i++] = *p;
    p++;
  }
  company_name[i] = '\0';
  p += strlen("job=");
  p++;
  i = 0;
  while(*p != '&' && *p != '\0')
  {
    job[i++] = *p;
    p++;
  }
  job[i] = '\0';
  p += strlen("date=");
  p++;
  i = 0;
  while(*p != '&' && *p != '\0')
  {
    date[i++] = *p;
    p++;
  }
  date[i] = '\0';
  p += strlen("status=");
  p++;
  i = 0;
  while(*p != '&' && *p != '\0')
  {
    status[i++] = *p;
    p++;
  }
  status[i] = '\0';
  return 0;
}

int main()
{
  //获取参数
  char buf[1024 * 4] = {0};
  fprintf(stderr, "获取失败\n");
  if(Get_info(buf) < 0)
  {
    fprintf(stderr, "Get_info failed!\n");
    return 1;
  }
  fprintf(stderr, "Get_info OK\n");
  fprintf(stderr, "Get_info OK:%s\n",buf);
  urldecode(buf);
  fprintf(stderr, "Get_info OK:%s\n",buf);

  char user_name[1024] = {0};
  char company_name[1024] = {0};
  char job[1024] = {0};
  char date[1024] = {0};
  char status[1024] = {0};
  //char * all_info[N+1] = {user_name,company_name,job,date,status,NULL};

  separate_info(buf,user_name,company_name,job,date,status);

  fprintf(stderr, "buf:%s\n,user_name:%s\n,company_name:%s\n,job:%s\n,date:%s\n,status:%s\n",buf,user_name,company_name,job,date,status);


  //1.初始化 mysql 句柄
  MYSQL * connect_fd = mysql_init(NULL);
  //2.建立连接
  if(mysql_real_connect(connect_fd, "127.0.0.1", "root", "1", "ForOffer", 3306, NULL, 0) == NULL)
  {
    fprintf(stderr, "mysql_real_connect failed!\n");
    return 1;
  }
  fprintf(stderr, "\nconnet OK!\n");
  //处理字符集
  
  fprintf(stderr,"\n-----insert_info-------%d-----%s-----\n",__LINE__,__FUNCTION__);
  if ( mysql_set_character_set(connect_fd, "utf8"  )  ) 
  { 
    fprintf ( stderr , "错误, %s\n" , mysql_error( connect_fd)  ) ; 
  } 
  

//***************先查询该用户名是否以及该存在**************************
  char sql[1024 * 5] = {0};
  sprintf(sql,"  select name from user where name = '%s';",user_name);
  fprintf(stderr , "hhehhhhh:%s",sql);
  int ret = mysql_query(connect_fd, sql);
  if(ret < 0)
  {
    fprintf(stderr, "mysql_query failed!\n");
    return 1;
  }

  
    MYSQL_RES * result = mysql_store_result(connect_fd);
    if(result == NULL)
    {
      fprintf(stderr,"mysql_store_result failed!\n");
      return 1;
    }
    //a)先获取到几行，以及几列
    int rows = mysql_num_rows(result);
    fprintf(stderr,"\nrows:%d\n",rows);
    if(rows < 1)
    {//说明查找到的结果为空

      printf(" <html><head><meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\"> </head><body>请先注册\n<div style=\"padding-top:10px;padding-left:0px;\" ><from><input type=\"submit\"   value=\"去注册\"onclick=\"location.href='../login.html'\" /></from></div> </body></html>");
      return 0;
    }

 //   int name_exists = -1;
 //   fprintf(stderr, "\nrows: %d   fields :%d\n",rows,fields);
 //   for(i = 0 ; i < rows; ++i)
 //   {
 //     MYSQL_ROW row = mysql_fetch_row(result);
 //     for(j = 0 ; j < fields; ++ j)
 //     {
 //       name_exists = strcmp(user_name, row[1]);
 //       
 //       fprintf(stderr,"ret_1 :%d\n",name_exists);
 //         //如果该用户名已经存在了，就不能注册成功
 //       }
 //     }


    //如果该用户名没有使用过，就可以直接进行注册新用户
    //2.构建sql 语句（字符串拼接）

    char sql_replace[1024] = {0};
    //使用replace 存在就更新,不存在就插入
  sprintf(sql_replace, " replace into info (user_name ,company_name,job,date,status) values('%s','%s','%s','%s','%s');",\
      user_name,company_name,job,date,status);
  fprintf(stderr, "sql :%s\n ",sql);
  //sql注入问题没有解决
  
  //3.把sql语句发送到服务器上
  ret = mysql_query(connect_fd, sql_replace);
  if(ret < 0)
  {
    fprintf(stderr, "mysql_query failed!\n");
    mysql_close(connect_fd);
    return 1;
  }
  //5.断开连接（*****不能忘*****）
  mysql_close(connect_fd);
  printf(" <html><head><meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\"> </head><body>添加信息成功，去查看申请信息\n<div style=\"padding-top:10px;padding-left:0px;\" ><from method = \"get\" action=\"./show_info/\"><input type=\"submit\"   value=\"查看申请信息\"onclick=\"location.href='./show_info?name=%s'\" /></from></div> </body></html>",user_name);
  //execl("./login",NULL);
  return 0;
}
