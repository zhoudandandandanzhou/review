#include <stdio.h>
#include <stdlib.h>
#include <mysql/mysql.h>


int main()
{

  //1.初始化 mysql 句柄
  MYSQL * connect_fd = mysql_init(NULL);
  //2.建立连接
     //参数含义(参数为什么这么多)：
     //a)mysql 连接句柄
     //b)mysql 服务器的ip地址
     //c)用户名
     //d)密码
     //e)要访问的数据库名
     //f)端口号
     //g)NULL
     //h)标志位
  if(mysql_real_connect(connect_fd, "127.0.0.1", "root", "1", "ForOffer", 3306, NULL, 0) == NULL)
  {
    fprintf(stderr, "mysql_real_connect failed!\n");
    return 1;
  }
    fprintf(stderr, "connet OK!\n");
  //3.构建sql 语句（字符串拼接）
  const char * sql = "select * from user";
  //4.把sql语句发送到服务器上
  int ret = mysql_query(connect_fd, sql);
  if(ret < 0)
  {
    fprintf(stderr, "mysql_query failed!\n");
    return 1;
  }
  //5.遍历结果集（服务器响应的表结构）
    //按行遍历，每次取出一行，再依次把所有的列都取出来
    MYSQL_RES * result = mysql_store_result(connect_fd);
    if(result == NULL)
    {
      fprintf(stderr,"mysql_store_result failed!\n");
      return 1;
    }
    printf("<html>\n");
      //a)先获取到几行，以及几列
      int rows = mysql_num_rows(result);
      int fields = mysql_num_fields(result);
      //b)把表头信息获取到
        //调用一次获取到一个表头
      MYSQL_FIELD * f = mysql_fetch_field(result);
        while(f != NULL)
        {
          printf("%s\t",f->name);
          f = mysql_fetch_field(result);
        }
        printf("<br>");
      //c)把表内容获取到
       int i = 0 , j = 0 ;
       for(i = 0 ; i < rows; ++i)
       {
         MYSQL_ROW row = mysql_fetch_row(result);
         for(j = 0 ; j < fields; ++ j)
         {
           printf("%s\t",row[j]);
         }
         printf("<br>");
       }
       printf("<br>");
      
  //6.断开连接（*****不能忘*****）
  mysql_close(connect_fd);
    printf("</html>\n");
  return 0;
}
