#include <stdio.h>
#include <stdlib.h>
#include <mysql/mysql.h>
#include "./base_CGI.h"


int main()
{
  //获取参数
  char buf[1024 * 4] = {0};
  if(Get_info(buf) < 0)
  {
    fprintf(stderr, "Get_info failed!\n");
    return 1;
  }
  fprintf(stderr, "Get_info OK\n");

  char name[1024] = {0};
  char password[1024] = {0};
  char * p = buf;
  p += strlen("name=");
  int i = 0;
  while(*p != '&' && *p != '\0')
  {
    name[i++] = *p;
    p++;
  }
  name[i++] = '\0';
  p += strlen("password=");
  p++;
  i = 0;
  while(*p != '&' && *p != '\0')
  {
    password[i++] = *p;
    p++;
  }
  password[i] = '\0';


  //1.初始化 mysql 句柄
  MYSQL * connect_fd = mysql_init(NULL);
  //2.建立连接
     //参数含义(参数为什么这么多)：
     //a)mysql 连接句柄
     //b)mysql 服务器的ip地址
     //c)用户名
     //d)密码
     //e)要访问的数据库名
     //f)端口号
     //g)NULL
     //h)标志位
  if(mysql_real_connect(connect_fd, "127.0.0.1", "root", "1", "ForOffer", 3306, NULL, 0) == NULL)
  {
    fprintf(stderr, "mysql_real_connect failed!\n");
    return 1;
  }
  fprintf(stderr, "\nconnet OK!\n");

//***************先查询该用户名是否以及该存在**************************

  const char * sql_select = "select * from user";
  //sql语句发送到服务器上
  int ret = mysql_query(connect_fd, sql_select);
  if(ret < 0)
  {
    fprintf(stderr, "mysql_query failed!\n");
    return 1;
  }
  //.遍历结果集（服务器响应的表结构）
    //按行遍历，每次取出一行，再依次把所有的列都取出来
    MYSQL_RES * result = mysql_store_result(connect_fd);
    if(result == NULL)
    {
      fprintf(stderr,"mysql_store_result failed!\n");
      return 1;
    }
    //a)先获取到几行，以及几列
    int rows = mysql_num_rows(result);
    int fields = mysql_num_fields(result);
    //b)把表内容获取到
    i = 0 ;
    int j = 0 ;


    fprintf(stderr, "\nrows: %d   fields :%d\n",rows,fields);
    for(i = 0 ; i < rows; ++i)
    {
      MYSQL_ROW row = mysql_fetch_row(result);
      for(j = 0 ; j < fields; ++ j)
      {
        int name_1 = strcmp(name, row[1]);
        
        fprintf(stderr,"ret_1 :%d\n",name_1);
        if(name_1 == 0)
        {
          //如果该用户名已经存在了，就不能注册成功
      printf("<html><head><meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\">"
      " </head><body>\n该用户名已经存在，请重新换一个\n</body></html>");
        goto Flag;
        }
      }
    }

Flag :
    if(i < rows)
    {
      return 0;
    }
  //如果该用户名没有使用过，就可以直接进行注册新用户
  //2.构建sql 语句（字符串拼接）

  char sql[1024] = {0};
  sprintf(sql, "insert into user(name,password) values('%s','%s');",name,password);
  //sql注入问题没有解决
  
  //3.把sql语句发送到服务器上
  ret = mysql_query(connect_fd, sql);
  if(ret < 0)
  {
    fprintf(stderr, "mysql_query failed!\n");
    return 1;
  }
  //5.断开连接（*****不能忘*****）
  mysql_close(connect_fd);
  return 0;
}
