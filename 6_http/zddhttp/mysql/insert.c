#include <stdio.h>
#include <stdlib.h>
#include <mysql/mysql.h>
#include "./base_CGI.h"


int main()
{
  //获取参数
  char buf[1024 * 4] = {0};
  if(Get_info(buf) < 0)
  {
    fprintf(stderr, "Get_info failed!\n");
    return 1;
  }

  int id = 0;
  char name[1024] = {0};
  sscanf(buf, "id=%d&name=%s", &id, name);

  //1.初始化 mysql 句柄
  MYSQL * connect_fd = mysql_init(NULL);
  //2.建立连接
     //参数含义(参数为什么这么多)：
     //a)mysql 连接句柄
     //b)mysql 服务器的ip地址
     //c)用户名
     //d)密码
     //e)要访问的数据库名
     //f)端口号
     //g)NULL
     //h)标志位
  if(mysql_real_connect(connect_fd, "127.0.0.1", "root", "1", "Job", 3306, NULL, 0) == NULL)
  {
    fprintf(stderr, "mysql_real_connect failed!\n");
    return 1;
  }
    fprintf(stderr, "connet OK!\n");
  //3.构建sql 语句（字符串拼接）

  char sql[1024] = {0};
  sprintf(sql, "insert into test values(%d,'%s');", id,name);
  //sql注入问题没有解决
  
  //4.把sql语句发送到服务器上
  int ret = mysql_query(connect_fd, sql);
  if(ret < 0)
  {
    fprintf(stderr, "mysql_query failed!\n");
    return 1;
  }
  //5.断开连接（*****不能忘*****）
  mysql_close(connect_fd);
  return 0;
}
