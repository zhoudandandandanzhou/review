# pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>


static int Get_info(char * info)
{
  char * query_string = getenv("QUERY_STRING");
  char * cont_length = getenv("CONTENT_LENGTH");
  ssize_t length = 0;
  sscanf(cont_length,"%ld",&length);

  if(length != 0)
  {//不是GET请求
    fprintf(stderr,"query_string 等于NULL\n");
    //应该来处理body
    //先获取到cont_length

    fprintf(stderr,"下面是length\n");
    fprintf(stderr,"main函数中length%d\n",length);
    int i = 0;
    for(i =0 ; i < length; ++i)
      {
        read(0,&info[i],1);
      }
      fprintf(stderr,"mian 函数中执行info\n");
      fprintf(stderr,info);
      return 0;
  }
  else
  {//这种就是处理GET请求
    strcpy(info,query_string);
    fprintf(stderr,"query_string 不为空\n");
    return 0;
  }
  return -1;
}
