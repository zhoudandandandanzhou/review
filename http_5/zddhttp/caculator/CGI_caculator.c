#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include "./base_CGI.h"


int main()
{
  //因为已经进行程序替换，所以从标准输入读取数据相当于从管道中读取
  //往标准输出中写数据，相当于往标准输出中写
  
  //先获取信息，如果是GET请求及query_string,如果是POST请求，及body
  char info[1024] = {0};
  Get_info(info);
  fprintf(stderr,info);
  int a = 0;
  int b = 0;
  fprintf(stderr,"mian 函数中执行info\n");
  sscanf(info,"a=%d&b=%d",&a,&b);
  fprintf(stderr,info);
  int ret = a + b;
  printf("<html><h1>ret = %d</h1></html>",ret);

  fprintf(stderr,"计算完成并返回\n");
  return 0;
}
