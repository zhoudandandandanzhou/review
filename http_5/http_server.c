#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/sendfile.h>
#include <fcntl.h>

#include <pthread.h>
#include <signal.h>

#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

typedef struct sockaddr  sockaddr;
typedef struct sockaddr_in  sockaddr_in;

#define SIZE (5 * 1024)


//定义一个结构体用来保存HTTP请求信息
typedef struct  Http_Request 
{
  char first_line[SIZE];
  char * method;
  char * url;
  char * path;
  char * query_string;
  int cont_length;
}Http_Request;


int Read_Line(const int sock,char *first_line,int size)
{
  //按行读取socket中的数据
  //这里一行的结尾换行可能有这几种   \r   \r\n   \n 
  int index = 0;
  char ch = '\0';
  while(index < size)
  {
    if(recv(sock,&ch,1,0) <= 0)
    {
      return -1;
    }
    //1.处理读取是字符为 \r\n \r
    if (ch == '\r')
    {
      recv(sock,&ch,1,MSG_PEEK);
      //第四个参数为MSG_PEEK
      //表示只是读取数据，并不将数据从缓冲区中拿走
      if(ch == '\n')//表明如果是\r\n 就需要将\n拿走了 
        recv(sock,&ch,1,0);
      else
        ch = '\n';
    }
    //2.处理读取的字符为'\n'或者是其他字符
      first_line[index++] = ch;
      //读到'\n'就结束读取
      if(ch == '\n')
      {
        break;
      }
  }
  first_line[index] = '\0';
  return index;
}

//解析首行
int Split(char *first_line,const char * str,char *split_string[])
{
    printf("*****************%s****%d************\n",__FUNCTION__,__LINE__);
 char * ret =  NULL;  
 char * buf = NULL;
 int index = 0;
 //因为strtok是线程不安全的函数
 //自己内部会每次保存下来本次切分的字符串，当然这个字符串的空间不是栈上的,函数调用一次后会释放掉
 //这里的空间是静态区的，这样的话，如果其它线程也可以访问到
 //所以我们实现方法为strtok_r,这里的第三个参数为二级指针，我们在栈上开辟一个指针变量传过去
 //这样的话，我们就可以在每个线程栈上开辟空间来保存这个临时的字符串，就不会造成线程安全问题
 ret = strtok_r(first_line,str,&buf);
 while(ret != NULL)
 {
   split_string[index++] = ret;
   ret = strtok_r(NULL,str,&buf);
 }
 return index;
}

int Parse_First_Line(char * first_line,char ** method,char ** url)
{
    printf("*****************%s****%d************\n",__FUNCTION__,__LINE__);
  //主要是进行字符串的切分
  //按照空格将字符串切分为3部分
  char * split_string[10] = {NULL};
  int ret = Split(first_line," ",split_string);
  if(ret != 3)
  {
    printf("切分失败\n");
    return -1;
  }
  *method = split_string[0];
  *url = split_string[1];
  return 0;
}

int Parse_url(char *url,char **path,char **query_string)
{
    printf("*****************%s****%d************\n",__FUNCTION__,__LINE__);
  int len = strlen(url);
  int i = 0;
  *path = url;
  *query_string = NULL;
  for(i = 0;i < len+1;++i)
  {
    if(url[i] == '?')
    {
      url[i] = '\0';
      *query_string = url + i + 1;
      break;
    }
  }
  //若是没有遇到?，说明请求没有query_string
  return 0;
}

//读取header，获取Content-Length
int Read_Header(int sock,int *cont_length)
{
    printf("*****************%s****%d************\n",__FUNCTION__,__LINE__);
  char * content_str = (char *)"Content-Length: ";
  *cont_length = 0;
  while(1)
  {
    char buf[SIZE] = {0};
    int read_size = Read_Line(sock,buf,SIZE);
    if(read_size <= 0)
    {
      return -1;
    }
    if(strcmp(buf,"\n") == 0)
    {
      //读到空行，表示header 部分结束，结束读取
      break;
    }
    //printf("读取header :buf :%s\n",buf);

    if(strncmp(buf,content_str,strlen(content_str)) == 0)
    {
      *cont_length = atoi(buf+strlen(content_str));
      printf("Read_Header :cont_legnth：%d\n",*cont_length);
      //读到Content_Length 字段后，还应该继续读取，因为这次不讲数据读取完，
      //就会留在缓冲区里，粘包问题
    }
  }
  return 0;
}

//拼接文件的完整路径
int  Catch_File_Path(char *path,char *file_path)
{
  sprintf(file_path,"./zddhttp%s",path);
  int len = strlen(file_path);

  struct stat st;
  int ret = stat(file_path,&st);
  if(ret < 0)
  {
    return -1;
  }
  //如果拼接后的路径为文件，不用处理
  //若是一个目录，我们需要拼接上index.html
  if(file_path[len -1] == '/')
  {
    //说明一定是目录，默认访问的该目录下的index.html页面
    sprintf(file_path,"%sindex.html",file_path);
    return 0;
  }

  //如果路径为目录，这里用到linux 下的系统调用来实现判断文件类型是不是目录
  //代码在 windows 下不能跑过
  if(S_ISDIR(st.st_mode))
  {
    sprintf(file_path,"%s/index.html",file_path);
    return 0;
  }
  //不是目录的话，就是按照普通文件处理
 return 0; 
}

int Process_Static(int sock,Http_Request * req)
{
  //如果是静态页面，我们将zddhttp目录作为http服务器的根目录，根目录下的默认页面为index.html 
  //例如url为/img/fruits.jpg  我们就需要拼接文件路径成 ./zddhttp/img/fruits.jpg
  //若url为/ 或者 没有写 我们就让其访问index.html页面
  char file_path[SIZE] = {0}; 
  int ret = Catch_File_Path(req->path,file_path);
  if(ret < 0)
  {
    printf("拼接失败\n");
    return 404;
  }

  printf("*****************%s****%d************\n",__FUNCTION__,__LINE__);
  printf("拼接之后：file_path%s\n",file_path);

  const char * first_line = "HTTP/1.1 200 OK\n";
  const char * blank_line = "\n";
  send(sock,first_line,strlen(first_line),0);
  send(sock,blank_line,strlen(blank_line),0);
  int fd = open(file_path,O_RDONLY);
  struct stat st;
  stat(file_path,&st);
  sendfile(sock,fd,NULL,st.st_size);
  printf("发送成功\n");
  return 200;

}

int Process_CGI( int sock, Http_Request * req )
{
  printf("*****************%s****%d************\n",__FUNCTION__,__LINE__);
  //先创建匿名管道用户父子进程之间的通信
  int fd1[2];
  int fd2[2];
  if(pipe(fd1) < 0)
  {
    return 404;
  }
  if(pipe(fd2) < 0)
  {
    return 404;
  }
  //对每个文件描述符都赋予意义
  int child_read = fd1[0];
  int father_write = fd1[1];
  int father_read = fd2[0];
  int child_write = fd2[1];

  //创建子进程，使子进程进行程序替换
  pid_t pid = fork();
  if(pid < 0)
  {
    return 404;
  }
  if(pid > 0)
  {//父进程
    //(a)关闭掉不使用的文件描述符
    close(child_read);
    close(child_write);
    //(b)将GET请求的参数或者POST请求的body写入管道中
    
    ssize_t len = req->cont_length;
    printf("父进程中打印：cont_length ：%ld\n",len);
    if(req->query_string == NULL && req->cont_length != 0)
    {//说明是POST请求,处理body
      char buf[SIZE * 10];
      printf("处理body\n");
      int i =0 ;
      for(i =0; i < len ; ++i)
      {
        if( read(sock,&(buf[i]),1) < 0)
        {
          //没有读取完body内容
            return 404;
        }
      }
      printf("body:的内容，%s\n",buf);
      for(i = 0;i < len ; ++i)
      {
        if( write(father_write,&(buf[i]),1) < 0)
        {//没有写完
          return 404;
        }
      }
    }
    else
    {//说明是GET请求带有query_string
      if(req->query_string != NULL )
      {
        write(father_write,req->query_string,strlen(req->query_string)); 
      }
    } 
    //(c)构造响应的首行和空行
    
    const char * first_line = "HTTP/1.1 200 OK\n";
    const char * conten_type = "Content-Type: text/html; charset=UTF-8\n";
    const char * blank_line = "\n";

    //(d)从管道中读取子进程处理后的数据
    
    send(sock,first_line,strlen(first_line),0);
    send(sock,conten_type,strlen(conten_type),0);

    send(sock,blank_line,strlen(blank_line),0);
    char buf[SIZE * 4] = {0};
    ssize_t read_size =read(father_read,buf,sizeof(buf));
    printf("Father read_size :%lu\n",read_size);
    send(sock,buf,read_size,0);
    //sendfile(father_read,sock,NULL,SIZE);// TODO
    printf("数据发送完成\n");
    //(e)回收资源，因为已经忽略掉的SIGCHLD信
  }
  else
  {//子进程
    //(a)将标准输入和标准输出重定向(CGI程序语言广泛，支持标准输入和输出即可)
    dup2(child_read,0);
    dup2(child_write,1);
    //(b)设置环境变量，因为CGI协议规定将某些参数进行环境变量替换
    
    //设置REQUESRT_METHOD 为method
    setenv("REQUEST_METHOD",req->method,2);
    //设置ONTENT_LENGTH 为 cont_length
    char buf[100] = {0};
    sprintf(buf,"%d",req->cont_length);
    setenv("CONTENT_LENGTH",buf,2);
    //设置QUERY_STRING 为query_string
    setenv("QUERY_STRING",req->query_string,2);

    char file_path[SIZE] ={0};
    Catch_File_Path(req->path,file_path);

    //struct stat st;
    //stat(file_path,&st);
    //if( ! (S_IXUSR & st.st_mode)  )
    //{
    //  //该文件没有可执行权限，不用进行程序替换
    //  int fd = open(file_path,O_RDONLY);
    //  char ch= '\0';
    //  while(1)
    //  {
    //    if( read(fd, &ch,1) )
    //    {
    //      printf("%c",ch);
    //    }
    //  }
    //}
    //fprintf(stderr,file_path);

    //printf("我的页面被喵星人吃掉了\n");
    
    //(c)进行程序替换
    if( execl(file_path,NULL) < 0)
    {
      fprintf(stderr,"程序替换失败\n");
      //表示程序替换失败
      //应该让子进程进行退出，否则子进程会执行父进程的逻辑
      exit(0);
    }
    //剩下的处理业务相关的事情交给CGI程序来完成
  }
  close(child_read);
  close(child_write);
  close(father_read);
  close(father_write);
  return 200;
}

void Error(int sock)
{
  char * first_line = (char *)"HTTP/1.1 404 Not Found\n";
  char * blank_line = (char *)"\n";
  char * body = (char *)"<html><head><meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\">"
                        " </head><body>404 Not Found\n,页面错误\n</body></html>";
  send(sock,first_line,strlen(first_line),0);
  send(sock,blank_line,strlen(blank_line),0);
  send(sock,body,strlen(body),0);
  close(sock);
}
//************************************************************************

//1.(1)启动服务器
int server_start(const char * ip,const short port)
{
  //1.创建socket
  int sock = socket(AF_INET,SOCK_STREAM,0);
  if(sock < 0) 
  {
    perror("socket");
    return -1;
  }

  //将socket设置成为reused,确保服务器主动断开连接之后不会出现bind失败的问题
 int opt = 1;
  setsockopt(sock,SOL_SOCKET,SO_REUSEADDR,&opt,sizeof(opt));

  //2.绑定端口号
  sockaddr_in addr;
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = inet_addr(ip);
  addr.sin_port = htons(port);
  int ret = bind(sock,(sockaddr *)&addr,sizeof(addr));
  if(ret < 0)
  {
    perror("bind");
    return -2;
  }
  //3.使sock处于被动监听状态
  ret = listen(sock , 5);//这里的等待连接队列数量不要太大
  if(ret < 0)
  {
    perror("listen");
    return -3;
  }
  //4.将处于监听状态的socket返回
  return sock;
}//end server_start()



//*****************************  2  ***************************************
//***************************处理一次连接**********************************
void Process_Connection(int sock)
{
  printf("*****************%s****%d************\n",__FUNCTION__,__LINE__);
  Http_Request req;
  //对req进行初始化
  memset(&req,0,sizeof(req));

  int err = 200;
  
  //（1）读取首行信息(忽略处理http://字段)
  if(Read_Line(sock,req.first_line,sizeof(req.first_line)) < 0)
  {
    err = 404; 
    goto FLAG;
  }
  printf("first_line:%s",req.first_line);
    
  //（2）解析首行，分析出 method 和 url 
  if(Parse_First_Line(req.first_line,&req.method,&req.url) < 0)
  {
    err = 404; 
    goto FLAG;
  }
  printf("first_line:%s",req.first_line);
  printf("method:%s,url :%s",req.method,req.url);

  //（3）解析url,分析出path 和 query_string
  if(Parse_url(req.url,&req.path,&req.query_string) < 0)
  {
    err = 404; 
    goto FLAG;
  }
  printf("first_line:%s",req.first_line);
  printf("method:%s,url :%s\n",req.method,req.url);
  printf("path :%s,query_string :%s\n",req.path,req.query_string);
  //（4）读取header信息(只处理 content_length)
  if(Read_Header(sock,&req.cont_length) < 0)
  {
    err = 404; 
    goto FLAG;
  }
  printf("first_line:%s",req.first_line);
  printf("method:%s,url :%s\n",req.method,req.url);
  printf("path :%s,query_string :%s\n",req.path,req.query_string);

  //（5）根据请求构造响应//TODO
  //这里只处理POST和GET
  //因为浏览器不同，对HTTP请求的方法命名大小写有别，此处处理时应忽略大小写的

  //（a）处理静态页面请求(当请求的方法为 GET 并且 无 参数时)
  if(strcasecmp(req.method,"GET") == 0 && req.query_string == NULL)
  {
    err = Process_Static(sock,&req);
    printf("Get  OK\n");
  }
  //（b）处理动态页面请求(当请求的方法为 GET 并且 有 参数时)
  if(strcasecmp(req.method,"GET") == 0 && req.query_string != NULL)
  {
    err = Process_CGI(sock,&req);
    printf("Get  query_string\n");
  }
  //（c）处理动态页面请求(当请求的方法为 POST 方法时)
  if(strcasecmp(req.method,"POST") == 0 )
  {
    printf("开始处理POST请求\n");
    err = Process_CGI(sock,&req);
  }

FLAG:
  if(err == 404)
  {
    Error(sock);
  }
  close(sock);

  //（5）处理完之后，关闭连接
  //但是因为，若是服务器主动关闭连接，就会进入time_wait状态，此时仍旧占用5元组
  //设置socket为可重复使用的
}

//1.(2)线程入口函数，处理一次连接
void *Pthread_Entry(void * arg)
{
  int acc_socket = (int)arg; 
  //处理一次连接
  Process_Connection(acc_socket);
  return NULL;
}

//*****************************  1  ***************************************
//***************************启动服务器************************************
//***************************进行事件循环**********************************
void Http_Servr_Start(const char * ip,const short port)
{
  //1.(1)启动服务器
  int listen_socket = server_start(ip,port);
  if(listen_socket < 0)
  {
    printf("server start failed!\n");
    return;
  }
  printf("server start OK!\n");
  //1.(2)进行事件循环
  //此处采用多线程方式进行处理多个客户端的连接
  //TODO  该为epoll
  while(1)
  {
    //（a）与客户端建立连接
    sockaddr_in peer;
    socklen_t peer_len = sizeof(peer);
    int acc_socket = accept(listen_socket,(sockaddr *)&peer,&peer_len);
    if(acc_socket < 0)
    {
      perror("accpet");
      continue;
    }
    //（b）创建线程去执行这次连接的任务
    pthread_t tid; 
    //这里为了保证线程安全，将acc_socket进行值传递，保证每个线程栈上的变量相互独有
    pthread_create(&tid , NULL, Pthread_Entry,(void *)acc_socket);
    //进行线程分离，保证线程资源正确回收,防止内存泄露
    pthread_detach(tid);

  }

}
//main函数模块
int main(int argc,char * argv[])
{
  //判断命令行参数的正确性
  if(argc != 3)
  {
    printf("Usage : ./http_server [ip]  [port]\n");
    return 1;
  }

  //忽略掉SIGCHLD信号，让操作系统负责回收子进程资源
  signal(SIGCHLD,SIG_IGN); 


  //***************************启动服务器************************************
  //***************************进行事件循环**********************************
  Http_Servr_Start(argv[1],atoi(argv[2]));
  
  return 0;
}//end main()
