#include <stdio.h>
#include <iostream>
using namespace std;

template <class T>

class Singleton_2
{
  public:
    static T * GetInstance()//第一次使用时 调用GetInstance()为T 类对象申请内存
    {
      if (_inst == NULL)
      {
        //LOCK  伪代码 加锁
        if (_inst == NULL)
        {
          _inst = new T();
        }
        //UNLOCK  伪代码  解锁
        return _inst;
      }
    }

  private:
    volatile static T * _inst;//只是一个指针,需要初始化为NULL
};


//其实也是一种延时加载

//这种单例模式并不是一个线程安全模式，因为在判断_inst是否为空和开辟空间并不是原子操作
//若是两个线程都执行到判断这一步，就会创建两个T 类对象,就会出现逻辑错误
//所以要在判断和创建对象时加上互斥锁
//有为了效率问题，我们只需要在_inst为空的时候再进行加锁
//又为了防止编译器过度优化，我们确保每次判断的时候值都是从内从中读取的，我们需要对私有成员加上 volatile关键字


