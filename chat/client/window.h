#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <unordered_set>
#include <deque>

#include <ncurses.h>
#include <locale.h>
namespace client
{

  class Window
  {
    public:
      //1.在构造函数中对窗口设置进行初始化
      Window();
      //2.在析构函数中对窗口设置进行恢复
      ~Window();
      //3.构造聊天界面的标题行窗口
      void Header_Win();
      //4.构造左部的消息显示窗口
      void Message_Win(); 
      //5.构造右部的在线好友列表窗口
      void Online_List_Win();
      //6.构造聊界面的底部输入窗口
      void Input_Win();
      //7.往窗口中放置字符串
      void Put_String_To_Win(WINDOW * win,int y,int x,const std::string & str);
      //8.从窗口中获取字符串
      void Get_String_From_Win(WINDOW * win,std::string &str);
      //9.添加一条消息到双端队列中
      void Add_Msg(const std::string & name,const std::string & words);
      //10.添加一个好友到好友列表中
      void Add_Friend(const std::string & nmae,const std::string & gender,const std::string & age);
      //11.从好友列表中删除一个好友
      void Del_Friend(const std::string & name,const std::string & gender,const std::string & age);
      
    //private:
      WINDOW * _header_win;
      WINDOW * _message_win;
      WINDOW * _online_list_win;
      WINDOW * _input_win;

      std::unordered_set<std::string> _online_list;
      std::deque<std::string> _msg;

  };//end class Window
}//end client namespace
