#pragma once 

#include "api.hpp"
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

namespace  client
{
  class Client
  {
    public:
      //1.启动客户端并与服务器建立连接
      int Init_Client(const std::string &ip,const short & port);
      
      //2.设置客户端信息
      //此处的客户端信息为每次客户端登陆的时候，自己输入的信息
      //信息包含用户的姓名，和性别 
      //TODO
      int Set_Client_Info(const std::string & name,const std::string & gender,const std::string & age);
      
      //3.向服务器发送消息
      int Send_Msg(const std::string &info);
      
      //4.接收服务器发来的消息
      int Recv_Msg(server::Msg & msg);
      
    private:
      //这里将 文件描述符、服务器addr、都定义为成员变量，为了方便所有的接口都可以直接使用
      int _sock;//文件描述符用来进行数据的发送和接收
      sockaddr_in _server_addr;
      //因为用户的信息(昵称和性别和年龄)是再一次聊天种不会再改变的，而且还要经常用到，所以这里也要将这些信息设置为成员变量
      std::string _name;
      std::string _gender;
      std::string _age;
      
  };//end Client

}//end client namespace

