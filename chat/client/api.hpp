#pragma once
#include <string>
#include <jsoncpp/json/json.h>


//******************************服务器和客户端实现交互接口*********************
//******************************约定应用层协议**********************************
//******************************由服务器端维护*********************************

namespace server
{
  struct Msg
  {

    //此处使用Json来实现字符串的序列化和反序列化
    //序列化
    void Serialize(std::string & output)
    {
      //Json::Value 用来保存数据，可以看成是一个一个是键值对
      Json::Value value;
      value["name"] = name;
      value["gender"] = gender;
      value["age"] = age;
      value["cmd"] = cmd;
      value["words"] = words;
      //Json::FastWriter 用来序列化
      //类似的接口还有StyledWrite 会控制数据的格式
      Json::FastWriter writer;
      //virtual std::string write(const Value& root);
      output = writer.write(value);
    }

    //反序列化
    void UnSerialize(const std::string & input)
    {
      //Json::Value 用来保存数据
      Json::Value value;
      //这个类用来进行反序列化
      Json::Reader reader;
      //parse(const std::string& document, Value& root, bool collectComments = true);
      reader.parse(input,value);
      //这里的第三个参数默认为为true，默认的将你的注释也进行处理
      
      //再进行对成员及进行赋值的时候，为了可靠性，应该要判断是否为自己想要的数据类型
      //这里的使用也并不像map那样value["key"]并不是返回字符串，需要再调用asString()接口转换
      if(value["name"].isString())
      {
        name = value["name"].asString();
      }

      if(value["gender"].isString())
      {
        gender = value["gender"].asString();
      }

      if(value["age"].isString())
      {
        age = value["age"].asString();
      }

      if(value["cmd"].isString())
      {
        cmd = value["cmd"].asString();
      }

      if(value["words"].isString())
      {
        words = value["words"].asString();
      }

    }
      std::string name;
      std::string gender;
      std::string age;
      std::string cmd;
      std::string words;
      //这里的cmd用来表示自己的当前的状态
      //设置为"quit"表示退出聊天
      //否则为正常聊天情况

  };//end Msg

}//end server namespace
