#include "client.h"

#if 0
  //发送消息进程的入口函数
  void * Send_Entry(void * arg)
  {
    printf("****************************%d*******************\n",__LINE__);
    client::Client * client_ptr = reinterpret_cast<client::Client *>(arg); 
    while(true)
    {
    printf("****************************%d*******************\n",__LINE__);
      std::string str;
      printf(">>");
      std::cin>>str;
      client_ptr->Send_Msg(str);
    }
    return NULL;
  }


  //接收消息的进程入口函数
  void * Recv_Entry(void *arg)
  {
    printf("****************************进入REcv%d*******************\n",__LINE__);
    client::Client * client_ptr = reinterpret_cast<client::Client *>(arg); 
    while(true)
    {
    printf("****************************RECV开始循环%d*******************\n",__LINE__);
     server:: Msg msg;
     client_ptr->Recv_Msg(msg);
     std::cout<<"["<<msg.name<<"|"<<msg.gender<<"|"<<msg.age<<"]:"<<msg.words<<std::endl;
    }
    return NULL;

  }

  //main函数逻辑
  int main(int argc,char * argv[])
  {
    if(argc != 3)
    {
      printf("Usage: ./client  ip  port\n");
      return 1;
    }

    //1.进行初始化
    client:: Client client;
    client.Init_Client(argv[1],atoi(argv[2]));

    std::string  name;
    std::string  gender;
    std::string  age;
    printf("请输入聊天昵称：");
    std::cin>>name;
    printf("请输入您的性别：");
    std::cin>>gender;
    printf("请输入您的年龄：");
    std::cin>>age;

    //2.进行设置用户信息
    client.Set_Client_Info(name,gender,age);

    //3.创建两个线程分别进行发送收据和接收数据
    pthread_t send_tid,recv_tid;

    //这里需要将client对象传递过去，为了保证线程安全,安利来说不应该直接将client对象的地址传递过去
    //但是这里client的作用域为整个客户端进程，生命周期直到进程结束，所以此处这样是安全的
    //一般为了保证线程安全，我们采取以下措施
    //按值传递的方式（void * 32位系统下为4个字节,传递的数据不能大于4字节）
    //这里采用在堆上开辟空间，记得在线程入口函数中进行释放空间

    pthread_create(&send_tid,NULL,Send_Entry,&client);
    printf("****************************%d*******************\n",__LINE__);
    pthread_create(&recv_tid,NULL,Recv_Entry,&client);

    printf("****************************%d*******************\n",__LINE__);
    //4.进行线程等待，回收线程资源，防止内存泄露
    pthread_join(send_tid,NULL);
    pthread_join(recv_tid,NULL);
    printf("****************************%d*******************\n",__LINE__);

    return 0;
  }//end main()

#endif
