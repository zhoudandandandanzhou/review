#include "server.h"

int main(int argc,char * argv[])
{
  if(argc != 3)
  {
    //TODO
    //使用打印日志的函数
    printf("./server ip port\n");
    return 1;
  }
  server::Chat_Server server;
  //argv[1]为字符串，Start_Server 的第一个参数为string,
  //string (const char* s);
  server.Start_Server(argv[1],atoi(argv[2]));
  
  return 0;
}
