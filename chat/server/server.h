
#include <stdio.h>
#include <iostream>
#include <stdlib.h>

#include <string>
#include <vector>
#include <unordered_map>
#include <semaphore.h>

#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include "block_queue.hpp"
#include "api.hpp"

//作用于server的命名空间
namespace  server
{
  //chat_server 
  class Chat_Server
  {
    //定义一个context类用来保存好友列表的标识信息，其实也就是unordered_map 中的key
    struct context
    {
      std::string str;
      sockaddr_in addr;
    };

    public:
      //1.********************启动服务器*********************
      int Start_Server(const std::string &ip,const int & port);
      //2.********************进行接收消息***************************
      int Recevice_Msg(); 
      //3.********************进行向所有的好友都发送数据*************
      int BoardCastMsg();


    //************************数据成员*******************************
    private:
      //用于保存好友列表的unordered_map 
      //<key> 用来唯一标识一个用户，这里用用户的 IP + Port + name
      //不能只用IP 基于NAT 机制，同一局域网中的主机IP可能一样 我们还有加上port 结合NAPT机制
      std::unordered_map <std::string,sockaddr_in> _online_friends_list;
      void * _consum(void *);//消费者线程，用来接收数据
      void * _product(void *);//生产者线程，用来发送数据
      Block_Queue<context> _block_queue;//自定义一个阻塞队列，用来作为生产者消费者模型的交易场所
      
      int _sock ;//_sock是用来读取和发送数据的 
      //这个socket我们定义成成员变量，也是为了保证每个接口种都可以使用到
      //sockaddr_in _addr;//_peer用来保存对端的IP和Port等信息

      //增加新的好友或者更新好友列表
      void Adduser(const sockaddr_in & addr,const std::string & name);
      //删除一个用户
      void Deluser(const sockaddr_in & addr , const std::string &name);

      //向一个用户发送消息
      void Send_Msg(const sockaddr_in & addr,const std::string & str);

  };//end Chat_Server

}//end server namespace


