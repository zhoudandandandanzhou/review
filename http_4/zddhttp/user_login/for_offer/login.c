#include <stdio.h>
#include <sys/types.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <mysql/mysql.h>
#include "./base_CGI.h"
#include "./decode.h"

void Create_info_Header() 
{
  fprintf(stderr,"进入Create_info函数\n");
  printf(
      "<html>"
      "<head>"
      "<META http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">"
      "<title>投递岗位详情</title>"
      "<div style=\"padding-top:10px;padding-left:1190px;\" >" 
       " <input type=\"submit\"   value=\"修改信息\" onclick=\"location.href='./change_info/change_info.html'\" />"
        "<input type=\"submit\"   value=\"插入信息\" onclick=\"location.href='./change_info/change_info.html'\" />"
      "</div>"
      "<style type=\"text/css\">"
      "body{"
      "BACKGROUND-COLOR: #c0c0c0; MARGIN: 0px}"
      "</style>"
      "</head>"
      "<body>"
      "<TABLE border=\"0\" cellSpacing=\"1\" cellPadding=\"2\" width=\"95%%\" align=\"center\">"
      "<COLGROUP>"
      "<COL width=\"40\">"
      "<COL width=\"150\">"
      "<COL width=\"78\">"
      "<COL width=\"162\">"
      "<COL width=\"40\">"
      "<COL width=\"54\">"
      "<COL width=\"68\">"
      "</COLGROUP>"
      "<br>"
      "<h2 align=\"center\">投递岗位情况</h2>"
      "<TBODY>"
      "<TR height=\"22\" bgColor=\"#ccccff\">"
      "<TH height=\"39\" rowSpan=\"2\" width=\"40\">序号</TH>"
      "<TH rowSpan=\"2\" width=\"180\">公司名称</TH>"
      "<TH rowSpan=\"2\" width=\"145\">岗位名称</TH>"
      "<TH rowSpan=\"2\" width=\"108\">时间</TH>"
      "<TH rowSpan=\"2\" width=\"162\">状态</TH>"
      "</TR>"
      "<TR height=\"20\"></TR>"
      );
}

void Create_info_Tital()
{
  printf("</TBODY>"
      "</TABLE>"
      "</body>"
      "</html>");
}

int  Process_login(MYSQL *connect_fd,const char * name)
{
  char sql[1024 * 5] = {0};
  sprintf(sql,"select name from user where name = '%s'",name);

  int ret = mysql_query(connect_fd, sql);
  if(ret < 0)
  {
    return 1;
  }
  
    fprintf(stderr,"读取html文档结束\n");

    MYSQL_RES * result = mysql_store_result(connect_fd);
    if(result == NULL)
    {
      fprintf(stderr,"mysql_store_result failed!\n");
      return 1;
    }
    int rows = mysql_num_rows(result);

    if(rows < 1)
    {
      //说明用户表里面没有该用户，让用户去注册
      fprintf(stderr, "mysql_query failed!\n");
      printf(" <html><head><meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\"> </head><body>您还没有注册账户\n<div style=\"padding-top:10px;padding-left:0px;\" ><from><input type=\"submit\"   value=\"去注册\"onclick=\"location.href='../login.html'\" /></from></div> </body></html>");
      return 0;
    }
    char select_buf[1024] = {0};
    sprintf(select_buf,"select * from info where user_name = '%s';",name);
    fprintf(stderr," select_buf:%s\n",select_buf);
    int ret_select  = mysql_query(connect_fd, select_buf);
    if(ret_select < 0)
    {
      fprintf(stderr, "mysql_query failed\n");
      return 1;
    }
    //用户存在的话就将信息返回给用户
    Create_info_Header(); 
     MYSQL_RES * result_select = mysql_store_result(connect_fd);
    if(result_select == NULL)
    {
      fprintf(stderr,"mysql_store_result failed!\n");
      return 1;
    }
    rows = mysql_num_rows(result_select);
    int fields = mysql_num_fields(result_select);
    //b)把表内容获取到
    int i = 0 ;
    fprintf(stderr, "\nrows: %d   fields :%d\n",rows,fields);
    for(i = 0 ; i < rows; ++i)
    {
      fprintf(stderr,"循环发送数据\n");
      MYSQL_ROW row = mysql_fetch_row(result_select);
      printf("<TR height=\"34\" bgColor=\"#ffffff\">");
      printf("<TD width=\"34\" width=\"40\">%d</TD>",i);
      printf("<TD width=\"180\" >%s</TD>",row[1]);
      printf("<TD width=\"145\"noWrap >%s</TD>",row[2]);
      printf("<TD width=\"108\">%s</TD>",row[3]);
      printf("<TD width=\"162\">%s</TD>",row[4]);
      printf("</TR>");
    }
    //将html文件的尾部发送过去
///    printf("  </TBODY></TABLE></body></html>");
   Create_info_Tital();
  return 0;  
}

int main()
{
  //获取参数
  char buf[1024 * 4] = {0};
  if(Get_info(buf) < 0)
  {
    fprintf(stderr, "Get_info failed!\n");
    return 1;
  }
  fprintf(stderr, "Get_info OK\n");

  //解码
  fprintf(stderr, "\n解码前：buf%s\n",buf);
  urldecode(buf);
  fprintf(stderr, "\n解码后：buf%s\n",buf);

  char name[1024] = {0};
  char password[1024] = {0};
  char * p = buf;
  p += strlen("name=");
  int i = 0;
  while(*p != '&' && *p != '\0')
  {
    name[i++] = *p;
    p++;
  }
  name[i++] = '\0';
  p += strlen("password=");
  p++;
  i = 0;
  while(*p != '&' && *p != '\0')
  {
    password[i++] = *p;
    p++;
  }
  password[i] = '\0';
  fprintf(stderr, "name:%s   password:%s\n",name, password);

  //1.初始化 mysql 句柄
  MYSQL * connect_fd = mysql_init(NULL);
  if(mysql_real_connect(connect_fd, "127.0.0.1", "root", "1", "ForOffer", 3306, NULL, 0) == NULL)
  {
    fprintf(stderr, "mysql_real_connect failed!\n");
    return 1;
  }
  //处理字符集
  if ( mysql_set_character_set(connect_fd, "utf8"  )  ) 
  { 
    fprintf ( stderr , "错误, %s\n" , mysql_error( connect_fd)  ) ; 
  } 

  Process_login(connect_fd,name);
  //5.断开连接（*****不能忘*****）
  mysql_close(connect_fd);

  return 0;
}
//   <TR height="34" bgColor="#ffffff">
//     <TD height="34" width="40">7</TD>
//     <TD width="180">公司金融</TD>
//     <TD width="145" noWrap>金融学院</TD>
//     <TD width="108">王国红</TD>
//     <TD width="162">校级精品课程</TD>
//    </TR>


   //将构造的html文件写给父进程
///  int fd = open("./info.html",O_RDONLY);
///  if(fd < 0)
///  {
///    printf("open failed\n");
///  }
///  char c = '\0';
///  int read_size = read(fd , &c, 1);
///  fprintf(stderr, "read_size:%d\n",read_size);
///  while(read_size)
///  {
///    fprintf(stderr,"读取到：%c",c);
///    read_size = read(fd, &c, 1);
///    printf("%c",c);
///  }


  //  int fd = open("./info.html",O_RDONLY);
  //  if(fd < 0) 
  //  {
  //    fprintf(stderr, "open failed\n");
  //    perror("open");
  //    fprintf(stderr, "errno: %d\n",errno);
  //  }

  //    char c = '\0';
  //    ret = read(fd, &c,1);
  //    fprintf(stderr,"##################\n   fd : %d\n ret :%d\n",fd,ret);
  //    fprintf( stderr,"%c",c);
  //    printf("%c",c);
  //  while(ret)
  //  {
  //    ret = read(fd, &c,1);
  //    fprintf(stderr,"ret %d\n",ret);
  //    fprintf(stderr,"##################\n");
  //    fprintf( stderr,"%c",c);
  //    printf("%c",c);
  //  }

///   close(fd);
