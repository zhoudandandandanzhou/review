#include <stdio.h>
#include <sys/select.h>
#include <fcntl.h>
#include <unistd.h>

//使用select 检测标准输入
int main()
{
  fd_set read_set;
  int fd = 0;//stdin
  FD_ZERO(&read_set);
  FD_SET(fd,&read_set);

  while(1)
  {
    int ret = select(fd + 1,&read_set,NULL,NULL,NULL);
    //此处只检测读就绪，写和异常不用理会，并设置为阻塞式检测
    if(ret < 0)
    {
      perror("select");
      continue;
    }
    printf("ret : %d\n",ret);
    if(FD_ISSET(fd,&read_set) == 0)
    {
      //说明出错
      printf("error!\n");
      continue;
    }
    else
    {
      char buf[1024] = {0};
      if(read(fd,buf,sizeof(buf)-1) > 0)
      {
        printf("%s\n",buf);
      }
    }
    //一次之后,重新设置read_set
    FD_ZERO(&read_set);
    FD_SET(fd,&read_set);
  }

  return 0;
}
