#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <iostream>
using namespace std;

//extern int fcntl (int __fd, int __cmd, ...);

int SetNoBlock(int fd)
{
  //使用F_GETFL将文件描述符状态返回
  int f1 = fcntl( fd,F_GETFL);
  if(f1< 0)
  {
    printf("get failed\n");
    return -1;
  }
  //使用F_SETFL将文件描述符状态设置为非阻塞
  fcntl(fd,F_SETFL,fd | O_NONBLOCK);
  return 0;
}

int main()
{

  //实现非阻塞式从标准输入读取数据
  if(SetNoBlock(0) == -1)
  {
    return 1;
  }
  char buf[1024] = {0};

  while(1)
  {
    ssize_t read_size = read(0,buf,sizeof(buf)-1);
    if(read_size < 0)
    {
      sleep(1);
      printf("read_size :%ld ,errno %d\n",read_size,errno);
      strerror(errno);
      continue;
    }
    if(read_size == 0)
    {
      printf("read done\n");
      return 0;
    }
    else
    { 
      printf("user say : %s\n",buf);
    }
  }

  return 0;
}
