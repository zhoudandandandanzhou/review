#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>


int main()
{
  //因为已经进行程序替换，所以从标准输入读取数据相当于从管道中读取
  //往标准输出中写数据，相当于往标准输出中写
  
  char * query_string = getenv("QUERY_STRING");
  int a = 2;
  int b = 4;
  sscanf(query_string,"a=%d&b=%d",&a,&b);
  fprintf(stderr,query_string);
  int ret = a + b;
  int fd = open("./log.txt",O_WRONLY);
  if(fd < 0)
  {
    fprintf( stderr,"创建文件失败\n");
  }
  write(fd,&a,sizeof(a));
  write(fd,&b,sizeof(b));
  write(fd,&ret,sizeof(ret));
  close(fd);
  printf("<html><h1>ret = %d</h1></html>",ret);

  fprintf(stderr,"计算完成并返回\n");
  return 0;
}
