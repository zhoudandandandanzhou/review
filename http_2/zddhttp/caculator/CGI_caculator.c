#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>


void Get_info(char * info)
{
  char * query_string = getenv("QUERY_STRING");
  char * cont_length = getenv("CONTENT_LENGTH");
  ssize_t length = 0;
  sscanf(cont_length,"%ld",&length);

  if(length != 0)
  {//不是GET请求
    fprintf(stderr,"query_string 等于NULL\n");
    //应该来处理body
    //先获取到cont_length

    fprintf(stderr,"下面是length\n");
    fprintf(stderr,"main函数中length%d\n",length);
    int i = 0;
    for(i =0 ; i < length; ++i)
      {
        read(0,&info[i],1);
      }
      fprintf(stderr,"mian 函数中执行info\n");
      fprintf(stderr,info);
  }
  else
  {//这种就是处理GET请求
    strcpy(info,query_string);
    fprintf(stderr,"query_string 不为空\n");
  }
}

int main()
{
  //因为已经进行程序替换，所以从标准输入读取数据相当于从管道中读取
  //往标准输出中写数据，相当于往标准输出中写
  
  //先获取信息，如果是GET请求及query_string,如果是POST请求，及body
  char info[1024] = {0};
  Get_info(info);
  fprintf(stderr,info);
  int a = 0;
  int b = 0;
  fprintf(stderr,"mian 函数中执行info\n");
  sscanf(info,"a=%d&b=%d",&a,&b);
  fprintf(stderr,info);
  int ret = a + b;
  printf("<html><h1>ret = %d</h1></html>",ret);

  fprintf(stderr,"计算完成并返回\n");
  return 0;
}
