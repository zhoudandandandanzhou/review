#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <sys/poll.h>

//poll一次等待的最多的文件描述符的个数
const int Max_size = 1024;



//启动server
int start_server(const char * ip,short port)
{
  //创建socket
  int sock = socket(AF_INET,SOCK_STREAM,0);
  if(sock < 0)
  {
    perror("socket");
    return -1;
  }
  //绑定ip和端口号
  sockaddr_in addr;
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = inet_addr(ip);
  addr.sin_port = htons(port);
  int ret = bind(sock,(sockaddr *)&addr,sizeof(addr));
  if(ret < 0)
  {
    perror("bind");
    return -1;
  }
  //使sock处于被动监听状态
  ret = listen(sock,5);  
  if(ret < 0)
  {
    perror("listen");
    return -1;
  }
  return sock;
}


//对poll_list 进行初始化
void init_poll_list(pollfd *poll_list,int max_size)
{
  int i = 0;
  for(i = 0; i < max_size;++i )
  {
    poll_list[i].fd = -1;
    poll_list[i].events = 0;
    poll_list[i].revents = 0;
  }
}

//对要关注的文件描述符进行设置
//此时只是关注都就绪的文件描述符
void add_poll_list(pollfd *poll_list,int max_size,int fd)
{
  int i = 0;
  for(i = 0;i < max_size ; ++i)
  {
    if(poll_list[i].fd == -1)
    {
      poll_list[i].events = POLLIN;
      poll_list[i].fd = fd;
      break;
    }
  }
}


//main函数
int main(int argc,char * argv[])
{
  if(argc != 3)
  {
    printf("Usage ./server ip port\n");
    return 1;
  }
  
  int listen_socket = start_server(argv[1],atoi(argv[2]));  
  if(listen_socket < 0)
  {
    printf("server start falid\n");
    return 2;
  }
  printf("server start ok\n");
  
  //定义一个pollfd数组
  pollfd poll_list[Max_size];  
  //对pollfd数组进行初始化
  init_poll_list(poll_list,Max_size);   
  //将listen_socket进行设置
  add_poll_list(poll_list,Max_size,listen_socket);

  while(true)
  {
    //借助poll实现等待文件描述符状态
    int ret = poll(poll_list,Max_size,-1);
    //printf("poll  done\n");
    if(ret < 0)
    {
      perror("poll");
      return -1;
    }
    if(ret == 0)
    {
      printf("time out\n");
    }

    //下面就是poll正确返回
    int i = 0;
    for(i = 0; i < Max_size;++i)
    {
      if(poll_list[i].fd == -1)
      {
        //无效的文件描述符
        continue;
      }

      if(poll_list[i].revents != POLLIN)
      {
        //该文件描述符的读事件还没有就绪
        continue;
      }

      //处理已经就绪的文件描述符
      sockaddr_in peer;
      socklen_t peer_len = sizeof(peer);
      if(poll_list[i].fd == listen_socket)
      {
        //printf("listen_socket  ok\n");
        //说明listen_socket已经都就绪
        //那么就要对listen_socket进行处理
        int new_socket = accept(listen_socket,(sockaddr *)&peer,&peer_len);
        if(new_socket < 0)
        {
          perror("accpet");
          continue;
        }
        //已经获得new_socket 那么就将new_socket进行在poll_list中设置 
        //继续循环，使poll继续等待new_socket的读事件就绪
        add_poll_list(poll_list,Max_size,new_socket);
        continue;
      }
      else
      {
        //printf("new_socket  ok\n");
        //这种情况就是new_socket已经就绪
        //我们就看可以开始进行读事件了
        char buf[1024] = {0};
        ssize_t read_size = read(poll_list[i].fd,buf,sizeof(buf) - 1);
        if(read_size < 0)
        {
          perror("read");
          close(poll_list[i].fd);
          //从pollfd数组中删除不再关注的文件描述符
          poll_list[i].fd = -1;
          continue;
        }
        if(read_size == 0)
        {
          printf("read done\n");
          close(poll_list[i].fd);
          //从pollfd数组中删除不再关注的文件描述符
          poll_list[i].fd = -1;
          continue;
        }
        //这种情况就是正确读取到内容了
        //接收客户端发来的消息
        buf[read_size] = '\0';
        printf("[client %s:%d .%d]say : %s",inet_ntoa(peer.sin_addr),ntohs(peer.sin_port),poll_list[i].fd,buf);

        //这里是回显服务器
        //然后将读取到的内容写回给客户端
        write(poll_list[i].fd,buf,strlen(buf));
        //这里只读一次，是因为等待下一次文件描述符就绪应该让poll来等待
      }
    }
  }//end while()

  return 0;
}//end main
